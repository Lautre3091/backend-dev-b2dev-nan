package com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PowerDTO {
    private Long id;
    private String name;
    private String description;
}
