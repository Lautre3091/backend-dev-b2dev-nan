package com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest;

import com.campusacademy.b2dev.backenddeveloppementsupport.models.Power;
import com.campusacademy.b2dev.backenddeveloppementsupport.repositories.PowerRepository;
import com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest.dto.PowerDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "powers")
@RequiredArgsConstructor
public class PowersController {

    private final PowerRepository powerRepository;

    @GetMapping
    public ResponseEntity<List<PowerDTO>> getAllPowers() {
        return ResponseEntity.ok(this.powerRepository.findAll()
                .stream()
                .map(power -> new PowerDTO(power.getId(), power.getName(), power.getDescription()))
                .collect(Collectors.toList()));
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<PowerDTO> createPowers(@RequestBody PowerDTO powerDTO) {
        Power power = new Power();
        power.setName(powerDTO.getName());
        power.setDescription(powerDTO.getDescription());

        Power createdPower = this.powerRepository.save(power);
        PowerDTO powerDTO1 = new PowerDTO(createdPower.getId(), createdPower.getName(), createdPower.getDescription());
        return ResponseEntity.ok(powerDTO1);
    }
    
    @DeleteMapping(path = "{id}") // => DELETE /powers/2
    public void deletePower(@PathVariable Long id) {
        this.powerRepository.deleteById(id);
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<PowerDTO> createPowers(@PathVariable Long id, @RequestBody PowerDTO powerDTO) {
        Power power;
        try {
            power = this.powerRepository.getOne(id);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.unprocessableEntity().build();
        }
        power.setName(powerDTO.getName());
        power.setDescription(powerDTO.getDescription());
        Power updatedPower = this.powerRepository.save(power);
        return ResponseEntity.ok(new PowerDTO(updatedPower.getId(), updatedPower.getName(), updatedPower.getDescription()));
    }
}
