package com.campusacademy.b2dev.backenddeveloppementsupport.repositories;

import com.campusacademy.b2dev.backenddeveloppementsupport.models.Power;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PowerRepository extends JpaRepository<Power, Long> {

}
